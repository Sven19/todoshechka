const express = require('express');
const bodyParser = require('body-parser');
const app = express();



app.use(bodyParser.json());

app.use('/api/task', require('./routes/api/task'));
app.use('/api/user', require('./routes/api/user'));
app.use('/api/auth', require('./routes/api/auth'));

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
