const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3');
const is = require('is_js');
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/auth');

const db = new sqlite3.Database('todoshechka.sqlite');


const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: 'smtp.yandex.ru',
  port: 465,
  secure: true,
  auth: {
    user: 'no-reply.4obia123@yandex.ru',
    pass: '123321q'
  }
});


// @route   POST api/auth
// @desc    Auth user
// @uccess  Public
router.post('/', (req, res) => {
  const { email, password } = req.body;

  if(!email || !password) {
    return res.status(400).json({msg: 'Пожалуйста, заполните все поля'});
  }

  if(!is.email(email)){
    return res.status(400).json({msg: 'Введите корректный email'});
  }

  const auth = new Promise((resolve, reject) => {
    db.get("SELECT * FROM `user` WHERE `email` = ?", [email], (err, user) => {
      if(!user) {
        reject(`Логин или пароль введены неверно`);
      } else {

        bcrypt.compare(password, user.password)
          .then(isMatch => {
            if(!isMatch) reject(`Логин или пароль введены неверно`);
            if(user.confirm) reject(`Пожалуйста, подтвердите email`);

            jwt.sign(
              { id: user.id },
              config.get('jwtSecret'),
              { expiresIn: 3600 },
              (err, token) => {
                if(err) reject(err);
                return res.send({
                  token,
                  user: {
                    id: user.id,
                    name: user.name,
                    email: user.email
                  }
                });
              }
            )
          })
      }
    });
  })
    .catch(err => {
      return res.status(400).json({msg: err});
    })
});



// @route   POST api/auth/registration
// @desc    Register New User And Company
// @uccess  Public
router.post('/registration', (req, res) => {

  const { name, email, password, admin, access } = req.body;

  if(!name || !email || !password) {
    return res.status(400).json({msg: 'Пожалуйста, заполните все поля'});
  }

  if(!is.email(email)){
    return res.status(400).json({msg: 'Введите корректный email'});
  }

  const confirmToken = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);

  const registration = new Promise((resolve, reject) => {
    db.all("SELECT * FROM `user` WHERE `email` = ?", [email], (err, rows) => {
      if(rows.length) {
        reject(`Email ${email} уже зарегистрирован`);
      } else {
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(password, salt, (err, hash) => {
            if (err) reject(err);
            resolve(hash);
          })
        });
      }
    });
  })
    .then(hashPassword => {
      db.run("INSERT INTO `user` (`name`, `email`, `password`, `company`, `admin`, `access`, `confirm`) VALUES (?, ?, ?, 1, ?, ?, ?)", [name, email, hashPassword, admin, access, confirmToken],
        (err) => {
          if (err) throw err;
        });
    })
    .then(resData => {
      const mailOptions = {
        from: 'Тудушечка <no-reply.4obia123@yandex.ru>',
        to: email,
        subject: 'Подтверждение email',
        html: `<h1>Здравствуйте!</h1>
                <p>Ваш email был указан при регистрации в сервисе тудушечка. Пожалуйста, подтвердите свой email перейдя по <a href="http://localhost:3000/confirm/${confirmToken}">ссылке</a>.</p>
                <p><a style="display: inline-block; padding: 8px 20px; background: #d1ecf1; border-radius: 5px; color: #0c5460; font-size: 16px;" href="http://localhost:3000/confirm/${confirmToken}">Подтвердить email</a></p>`
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error);
          return res.status(400).json({msg: 'Ошибка регистрации. Пожалуйста, обратитесь в техническую поддержку'});
        }
        else {
          console.log('Email sent: ' + info.response);
          return res.send({email});
        }
      });
    })
    .catch(err => {
      return res.status(400).json({msg: err});
    })
});


// @route   GET api/auth/confirm/:token
// @desc    Confirm Email by Token
// @uccess  Public
router.get('/confirm/:token', (req, res) => {
  db.get('SELECT * FROM `user` WHERE `confirm` = ?', [req.params.token], (err, user) => {
    if (err) return res.status(400).json({msg: err});
    if(!user) {
      res.send(false);
    } else {
      db.run(`UPDATE user SET confirm = '' WHERE id = ${user.id}`, err => {
        if (err) {
          res.send(false);
        } else {
          res.send(true);
        }
      });
    }
  });
});


// @route   GET api/auth/user
// @desc    Get User Info
// @uccess  Private
router.get('/user', authMiddleware, (req, res) => {
  db.get('SELECT * FROM `user` WHERE `id` = ?', [req.user.id], (err, user) => {
    return err ?  res.status(400).json({msg: err}) : res.send(user);
  });
});

module.exports = router;
