const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3');
const is = require('is_js');
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const auth = require('../../middleware/auth');

const db = new sqlite3.Database('todoshechka.sqlite');

// @route   GET api/user
// @desc    Get All Users
// @uccess  Private
router.get('/', auth, (req, res) => {
  db.all('SELECT `id`, `name`, `email` FROM user ORDER BY `id` DESC', (err, rows) => {
    console.log(rows);
    res.send(rows);
  });
});


// @route   DELETE api/user/:id
// @desc    Delete User by id
// @uccess  Private
router.delete('/:id', auth, (req, res) => {
  db.run("DELETE FROM `user` WHERE `id` = ?", [req.params.id],
    (err) => {
      if(err) console.log(err);
      else{
        res.send('success');
      }
    });
});


// @route   POST api/user
// @desc    Register new user
// @uccess  Private
router.post('/', auth, (req, res) => {
  const { name, email, password, admin, access } = req.body;

  if(!name || !email || !password) {
    return res.status(400).json({msg: 'Пожалуйста, заполните все поля'});
  }

  if(!is.email(email)){
    return res.status(400).json({msg: 'Введите корректный email'});
  }


  const registration = new Promise((resolve, reject) => {
    db.all("SELECT * FROM `user` WHERE `email` = ?", [email], (err, rows) => {
      if(rows.length) {
        reject(`Email ${email} уже зарегистрирован`);
      } else {
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(password, salt, (err, hash) => {
            if (err) reject(err);
            resolve(hash);
          })
        });
      }
    });
  })
    .then(hashPassword => {
      db.run("INSERT INTO `user` (`name`, `email`, `password`, `company`, `admin`, `access`) VALUES (?, ?, ?, 1, ?, ?)", [name, email, hashPassword, admin, access],
        (err) => {
          if (err) {
            reject(err);
          }
        });
    })
    .then(r => {
      db.get("SELECT * FROM `user` WHERE `email` = ?", [email], (err, row) => {
        return res.send(row);
      });
    })
    .catch(err => {
      return res.status(400).json({msg: err});
    })
});

module.exports = router;
