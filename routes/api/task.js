const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3');
const auth = require('../../middleware/auth');

const db = new sqlite3.Database('todoshechka.sqlite');

// @route   GET api/task
// @desc    Get All Tasks
router.get('/', (req, res) => {
  db.all('SELECT * FROM `task` ORDER BY `id` DESC', (err, rows) => {
    console.log(rows);
    res.send(rows);
  });
});


// @route   GET api/task/actual/:userId
// @desc    Get All Tasks
router.get('/actual/:userId', (req, res) => {
  db.all('SELECT * FROM `task` WHERE `user` = ? AND done = 0 ORDER BY `id` DESC', [req.params.userId], (err, rows) => {
    console.log(rows);
    res.send(rows);
  });
});

// @route   GET api/task/actual/:userId
// @desc    Get All Tasks
router.get('/done/:userId', (req, res) => {
  db.all('SELECT * FROM `task` WHERE `user` = ? AND done = 1 ORDER BY `id` DESC', [req.params.userId], (err, rows) => {
    console.log(rows);
    res.send(rows);
  });
});

// @route   GET api/task/:id
// @desc    Get Task by id
router.get('/:id', (req, res) => {
  const id = req.params.id;
  db.all(`SELECT * FROM task WHERE id = ${id}`, (err, rows) => {
    console.log(rows);
    res.send(rows);
  });
});

// @route   DELETE api/task/:id
// @desc    Delete Task by id
router.delete('/:id', auth, (req, res) => {
  db.run("DELETE FROM `task` WHERE `id` = ?", [req.params.id],
    (err) => {
    if(err) console.log(err);
    else{
      res.send('success');
    }
    });
});

// @route   POST api/task
// @desc    Insert New Task
router.post('/', (req, res) => {
  const deadline = req.body.date && req.body.time ? `${req.body.date} ${req.body.time}:00` : '';
  db.run("INSERT INTO `task` (`name`, `text`, `user`, `creator`, `deadline`, `create`) VALUES (?, ?, ?, ?, ?, DateTime('now'))", [req.body.name, req.body.text,req.body.user,req.body.user, deadline],
    (err) => {
      if (err) {
        console.log(err);
      }
      else {
        db.get("SELECT * FROM `task` WHERE `name` = ?", [req.body.name], (err, row) => {
          console.log(row);
          res.send(row);
        });
      }
    });
});

module.exports = router;