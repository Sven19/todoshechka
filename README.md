![Alt Text](https://psv4.userapi.com/c848432/u12918244/docs/d18/b558c96a1ed5/todoshechka_v0_2.gif)


запуск приложения 
### `npm run dev`


**API**

TASK:

> *GET api/task* (получить список всех задач)
> 
> *GET api/task/:id* (получить информацию по задаче по id)
> 
> *DELETE api/task/:id* (удалить задачу по id)
> 
> *POST api/task* (создать задачу)



USER:

> *GET api/user* (получить список всех пользователей)
> 
> *DELETE api/user/:id* (удалить пользователя по id)
> 
> *POST api/user* (зарегистрировать пользователя)
