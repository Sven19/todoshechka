import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from 'reactstrap';
import { connect } from 'react-redux';
import { addItem } from '../../../actions/itemActions';

class TaskModal extends Component {
  state = {
    modal: false,
    name: '',
    text: ''
  };

  toggle = () => {
    this.setState({modal: !this.state.modal});
  };

  onChange = e => {this.setState({ [e.target.name]: e.target.value });};

  onSubmit = e => {
    e.preventDefault();
    const newItem = {
      name: this.state.name,
      text: this.state.text
    };

    this.props.addItem(newItem);
    this.toggle();
  };

  render() {
    return(
      <div>
        <Button
          color="dark"
          style={{marginBottom: '2rem'}}
          onClick={this.toggle}
        >Новая задача</Button>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
        >
          <ModalHeader toggle={this.toggle}>Добавить новую задачу</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for="item">Название</Label>
                <Input
                 type="text"
                 name="name"
                 id="item"
                 onChange={this.onChange}
                />

                <Label for="text">Описание</Label>
                <Input
                  type="textarea"
                  name="text"
                  id="text"
                  onChange={this.onChange}
                />
                <Button
                  color="dark"
                  style={{marginTop: '1rem'}}
                >Сохранить</Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  item: state.item
});

export default connect(mapStateToProps, { addItem })(TaskModal);




