import React, { Component, Fragment } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { connect } from 'react-redux';
import { getItems, getDoneItems, deleteItem } from '../../../actions/itemActions';
import PropTypes from 'prop-types';
import TaskItem from '../TaskItem/TaskItem'
import { Tabs, Icon, Badge, Spin, Empty, Skeleton } from 'antd';
import styles from "../TaskForm/TaskForm.module.css";

const TabPane = Tabs.TabPane;

class TaskList extends Component {

  state = {
    loading: true
  };

  onDeleteClick = id => {
    this.props.deleteItem(id);
  };

  componentDidMount() {
    if (this.props.auth.user) {
      this.props.getItems(this.props.auth.user.id);
      this.props.getDoneItems(this.props.auth.user.id);
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { auth, item } = this.props;
    if (auth !== prevProps.auth) {
      if (auth.user) {
        this.props.getItems(this.props.auth.user.id);
        this.props.getDoneItems(this.props.auth.user.id);
      }
    }

    if (item.loading !== prevProps.item.loading) {
      this.setState({loading: item.loading});
    }
  }

  render() {
    const { items, doneItems } = this.props.item;
    const actual = (<Fragment><Icon type='ordered-list' /> Актуальные <Badge style={{marginLeft: "10px"}} count={items.length} /></Fragment>);
    const done = (<Fragment><Icon type="check-circle" /> Выполненные</Fragment>);


    return (
      <Tabs defaultActiveKey="1">
        <TabPane tab={actual} key="1">

          {this.state.loading ?
            <TransitionGroup className="task-list" style={{display: "flex",  flexDirection: "row", flexWrap: "wrap"}}>
              <TaskItem skelet="true" />
              <TaskItem skelet="true" />
              <TaskItem skelet="true" />
              <TaskItem skelet="true" />
            </TransitionGroup>
            : null
          }

          {!this.state.loading && !items.length ? <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={<span>Нет актуальных задач</span>}/> : null}

          {items.length && !this.state.loading ?
              <TransitionGroup className="task-list" style={{display: "flex",  flexDirection: "row", flexWrap: "wrap"}}>
                {items.map(item => (
                  <CSSTransition key={item.id} timeout={500} classNames="fade">
                      <TaskItem
                        onDelete={this.onDeleteClick.bind(this, item.id)}
                        item={item}
                      />
                  </CSSTransition>
                ))}
              </TransitionGroup>
            :null

          }
        </TabPane>


        <TabPane tab={done} key="2">
          {this.state.loading && <Spin/>}

          {doneItems.length && !this.state.loading ?
              <TransitionGroup className="task-list">
                {doneItems.map(({id, name, text}) => (
                  <CSSTransition key={id} timeout={500} classNames="fade">
                      <TaskItem
                        onDelete={this.onDeleteClick.bind(this, id)}
                      ><b>{name}</b> - {text}
                      </TaskItem>
                  </CSSTransition>
                ))}
              </TransitionGroup>
             :
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={<span>Нет выполненных задач</span>}/>
          }
        </TabPane>
      </Tabs>
    );
  }
}

TaskList.propTypes = {
  getItems: PropTypes.func.isRequired,
  getDoneItems: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  item: state.item,
  auth: state.auth
});

export default connect(mapStateToProps,
  { getItems, getDoneItems, deleteItem }
  )(TaskList);
