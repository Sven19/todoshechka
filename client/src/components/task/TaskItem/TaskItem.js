import React, { Component } from 'react';
import { Button } from "reactstrap";
import RemoveButton from "../../Common/RemoveButton/RemoveButton";
import styles from './TaskItem.module.css';
import { Card, Skeleton } from 'antd';

export default ({ onDelete, item, skelet }) => (

  <div style={{ padding: "5px", width: "33.3%" }}>
    <Card
      size="small"
      title={skelet ? <Skeleton active paragraph={{ rows: 0 }} /> : item.name}
      extra={skelet ? null : <RemoveButton onDelete={onDelete} /> }
    >
      <p>{skelet ? <Skeleton active paragraph={{ rows: 2 }}/> : item.text}</p>

      {!skelet && item.deadline && <p style={{color: "#9c9c9c", fontSize: "12px", marginBottom: "0"}}><b>Дедлайн:</b> {item.deadline}</p>}
    </Card>
  </div>

);
