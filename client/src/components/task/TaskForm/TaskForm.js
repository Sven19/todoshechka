import React, { Component } from 'react';
import { Alert, Input, Form, Icon, Button, Row, Tabs, DatePicker, TimePicker } from 'antd';
import { connect } from 'react-redux';
import { addItem } from '../../../actions/itemActions';
import styles from './TaskForm.module.css';

const { TextArea } = Input;
const TabPane = Tabs.TabPane;
class TaskForm extends Component {
  state = {
    name: '',
    text: '',
    date: '',
    time: '',
    error: ''
  };



  onChange = e => {this.setState({ [e.target.name]: e.target.value });};
  onChangeTime = (time, timeString) => {this.setState({time: timeString})};
  onChangeDate = (date, dateString) => {this.setState({date: dateString})};

  onSubmit = e => {
    e.preventDefault();
    const newItem = {
      name: this.state.name,
      text: this.state.text,
      date: this.state.date,
      time: this.state.time,
      user: this.props.user.id
    };

    if (!newItem.name) {
      this.setState({error: 'Укажите тему задачи'})
    } else {
      console.log(newItem);
      this.props.addItem(newItem);
      this.setState({name: '', text: '', error: '', time: '', date: ''});
    }
  };

  render() {
    return(
      <Tabs defaultActiveKey="1">
        <TabPane tab="Новая задача" key="1" className={styles.taskForm}>
          <Form onSubmit={this.onSubmit} >
            {this.state.error && <Alert type="error" message={this.state.error}/>}
            <Row>
              <Input size="large" name="name" onChange={this.onChange} placeholder="Тема" autocomplete="off" value={this.state.name}/>
            </Row>

            <Row>
              <TextArea name="text" onChange={this.onChange} rows={4} placeholder="Описание задачи" autocomplete="off" value={this.state.text}/>
            </Row>

            <Row>
              <p>Дедлайн:</p>
              <DatePicker name="date" onChange={this.onChangeDate} placeholder="Дата"/>
              <TimePicker name="time" onChange={this.onChangeTime} format='HH:mm' placeholder="Время" style={{marginLeft: "10px", width: "117px"}}/>
            </Row>

            <Button
              type="primary"
              htmlType="submit"
            >
              Создать
            </Button>
          </Form>
        </TabPane>
      </Tabs>

    );
  }
}

const mapStateToProps = state => ({
  item: state.item,
  user: state.auth.user
});

export default connect(mapStateToProps, { addItem })(TaskForm);




