import React, {Component, Fragment} from 'react';
import { Alert, Input, Form, Icon, Button } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../../../actions/authActions';
import { Redirect } from 'react-router-dom';

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
    msg: '',
  };

  static propTypes = {
    isAuthentificated: PropTypes.bool,
    error: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired
  };

  onChange = e => {this.setState({ [e.target.name]: e.target.value });};

  onSubmit = e => {
    e.preventDefault();

    const user = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.login(user);
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { error, auth } = this.props;
    if (error !== prevProps.error) {
      if (error.id === 'LOGIN_FAIL') {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg:null });
      }
    }
  }

  render() {
    const { isAuthentificated } = this.props.auth;
    if (isAuthentificated) {
      return <Redirect to='/task'/>;
    }

    return(
      <Fragment>

          <Form onSubmit={this.onSubmit}>
            {this.state.msg && <Alert type="error" message={this.state.msg} showIcon />}
            <Input name="email" onChange={this.onChange} prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="E-mail" autoComplete="off" />
            <Input name="password" onChange={this.onChange} type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Пароль" autoComplete="off" />

            <Button
              type="primary"
              htmlType="submit"
            >
              Войти
            </Button>
          </Form>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  isAuthentificated: state.auth.isAuthentificated,
  error: state.error,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { login }
)(LoginForm);
