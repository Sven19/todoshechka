import React, {Component, Fragment} from 'react';
import { Alert, Input, Form, Icon, Button } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { register } from '../../../actions/authActions';

class RegisterForm extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    msg: '',
    isNewUser: false
  };

  static propTypes = {
    isAuthentificated: PropTypes.bool,
    error: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    register: PropTypes.func.isRequired
  };

  onChange = e => {this.setState({ [e.target.name]: e.target.value });};

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };

    this.props.register(newUser);
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { error, auth } = this.props;
    if (error !== prevProps.error) {
      if (error.id === 'REGISTER_FAIL') {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg:null });
      }
    }

    if (auth !== prevProps.auth) {
      this.setState({ isNewUser: auth.isNewUser });
    }
  }

  render() {
    return(
      <Fragment>

        {this.state.isNewUser ?
          <Alert type="success" message="Вы успешно зарегистрировались в тудушечке!" description={`Для подтверждения перейдите по ссылке, отправленной на ваш e-mail ${this.state.email}`} showIcon /> :

          <Form layout="inline" onSubmit={this.onSubmit}>
            {this.state.msg && <Alert type="error" message={this.state.msg} showIcon />}
            <Input name="name" onChange={this.onChange} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Имя" autoComplete="off" />
            <Input name="email" onChange={this.onChange} prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="E-mail" autoComplete="off" />
            <Input name="password" onChange={this.onChange} type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Пароль" autoComplete="off" />

            <Button
              type="primary"
              htmlType="submit"
            >
              Зарегистрироваться
            </Button>
          </Form>

        }
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  isAuthentificated: state.auth.isAuthentificated,
  error: state.error,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { register }
  )(RegisterForm);
