import React, {Component, Fragment} from 'react';
import { Alert } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { checkConfirm } from '../../../actions/authActions';
import {Col, Row, Tabs} from "antd";
import styles from "../../../container/AuthPage.module.css";
import LoginForm from "../LoginForm/LoginForm";
import RegisterForm from "../RegisterForm/RegisterForm";

class Confirm extends Component {
  state = {
    isConfirm: null,
    loading: true
  };

  componentDidMount() {
    this.props.checkConfirm(this.props.token);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { isConfirm } = this.props;
    if (isConfirm !== prevProps.isConfirm) {
      this.setState({ isConfirm, loading: false });
    }
  }

  render() {
    if(this.state.loading ) {
      return null;
    }

    return(
      <Fragment>
            { this.state.isConfirm ?
                <Fragment>
                  <div style={{padding: "10px", marginBottom: "10px"}}>
                    <Alert type="success" message="Ваш аккаунт успешно активирован!" description="Теперь Вы можете войти в свой профиль и использовать все возможности Тудушечки" showIcon  />
                  </div>

                  <div className={styles.formBody}><LoginForm /></div>
              </Fragment>

              :
              <Fragment>
                <div style={{padding: "10px", marginBottom: "10px"}}>
                <Alert type="error" message="Ошибка при подтверждении." description="Пожалуйста, обратитесь в тех. подержку" showIcon />
                </div>

              </Fragment>
            }
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  isConfirm: state.auth.isConfirm,
});

export default connect(
  mapStateToProps,
  { checkConfirm }
)(Confirm);
