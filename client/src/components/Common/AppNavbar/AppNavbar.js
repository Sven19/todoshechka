import React, {Component, Fragment} from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container
} from 'reactstrap';
import { NavLink } from "react-router-dom";
import styles from "./AppNavbar.module.css";
import { logout } from '../../../actions/authActions';
import { connect } from 'react-redux';
import { Dropdown, Icon, Avatar } from 'antd';

const links = [
  {to: '/task', label: 'Задачи', exact: false},
  // {to: '/registration', label: 'Регистрация', exact: false},
];

class AppNavbar extends Component {

  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  logout = () => {
    this.props.logout();
    window.location = "/";
  };

  render() {
    const { isAuthentificated, user } = this.props.auth;


    const logoutButton = (
      <NavItem>
        <NavLink
          className={styles.logout}
          onClick={this.logout}
          to="#">
          Выход
        </NavLink>
      </NavItem>
    );


    const authLinks = (
      <Fragment>
        {
          links.map((link, index) => {
            return (
              <NavItem>
                <NavLink
                  className={styles.navlink}
                  to={link.to}
                  exact={link.exact}
                  activeClassName={styles.active}
                >{link.label}</NavLink>
              </NavItem>
            )
          })
        }

        <NavItem style={{marginTop: "-4px", marginLeft: "70px"}}>
          <Dropdown overlay={logoutButton}>
            <a className="ant-dropdown-link" href="#">
              { user && <span style={{color: "#abaeb0"}}><Avatar style={{ color: '#fff', backgroundColor: '#99c8ff', marginRight: "10px" }}>{ user.name[0].toUpperCase() }</Avatar> { user.name }</span> } <Icon type="down" />
            </a>
          </Dropdown>
        </NavItem>
      </Fragment>
    );

    const guestLinks = (
      <Fragment>
        <NavItem>
          <NavLink
            className={styles.navlink}
            to="#">
            Документация
          </NavLink>
        </NavItem>
      </Fragment>
    );

    return (
      <div>
        <Navbar color="dark" dark expand="sm" className="mb-5" style={{height: "76px"}}>
          <Container>
            <NavbarBrand href="/"><img src="../../image/logo.png" /></NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>

                { isAuthentificated ? authLinks : guestLinks}

              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
  mapStateToProps,
  { logout }
)(AppNavbar);

