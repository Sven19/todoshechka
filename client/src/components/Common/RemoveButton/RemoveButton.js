import React, { Component } from 'react';
import { Button } from "reactstrap";
import styles from './RemoveButton.module.css';

export default ({ onDelete }) => (
    <button type="button"
            onClick={onDelete}
            className={styles.removebtn}
    ><i className="fa fa-trash-o"></i></button>
);
