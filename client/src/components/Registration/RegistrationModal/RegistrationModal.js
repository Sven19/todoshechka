import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from 'reactstrap';
import { connect } from 'react-redux';
import { addUser } from '../../../actions/userActions';

class RegistrationModal extends Component {
  state = {
    modal: false,
    name: '',
    email: '',
    password: '',
    admin: 1,
    access: 0
  };

  toggle = () => {
    this.setState({modal: !this.state.modal});
  };

  onChange = e => {this.setState({ [e.target.name]: e.target.value });};

  onSubmit = e => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };

    this.props.addUser(newUser);

    this.toggle();
  };

  render() {
    return(
      <div>
        <Button
          color="dark"
          style={{marginBottom: '2rem'}}
          onClick={this.toggle}
        >Новый пользователь</Button>

        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
        >
          <ModalHeader toggle={this.toggle}>Добавить нового пользователя</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for="item">Имя</Label>
                <Input
                  type="text"
                  name="name"
                  id="item"
                  onChange={this.onChange}
                />

                <Label for="email">Email</Label>
                <Input
                  type="text"
                  name="email"
                  id="email"
                  onChange={this.onChange}
                />

                <Label for="password">Пароль</Label>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  onChange={this.onChange}
                />

                <Button
                  color="dark"
                  style={{marginTop: '1rem'}}
                >Сохранить</Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { addUser })(RegistrationModal);




