import React, { Component } from 'react';
import AppNavbar from './components/Common/AppNavbar/AppNavbar';
import { Container } from 'reactstrap';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import TasksPage from './container/TasksPage';
import RegistrationPage from './container/RegistrationPage';
import AuthPage from './container/AuthPage';
import ConfirmPage from './container/ConfirmPage';
import { loadUser } from './actions/authActions';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Provider } from 'react-redux';
import store from "./store";
import 'antd/dist/antd.css';

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="App">
            <AppNavbar />
            <Container>
              <Switch>
                <Route path="/confirm/:token" component={ConfirmPage}/>
                <Route path="/registration" component={RegistrationPage}/>
                <Route path="/task" component={TasksPage}/>
                <Route path="/" component={AuthPage}/>
              </Switch>
            </Container>
      </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
