import axios from 'axios';
import { returnErrors } from "./errorActions";
import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  ADD_USER, CHECK_CONFIRM
} from "./types";



export const login = user => dispatch => {
  axios
    .post('/api/auth/', user)
    .then(res =>
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      })
    )
    .catch(err => {
      dispatch(returnErrors(err.response.data, err.response.status, 'LOGIN_FAIL'));
      dispatch({
        type: LOGIN_FAIL
      })
    })
};

export const logout = () => {
  return {
    type: LOGOUT_SUCCESS
  }
};

export const checkConfirm = token => dispatch => {
  axios
    .get(`/api/auth/confirm/${token}`)
    .then(res =>
      dispatch({
        type: CHECK_CONFIRM,
        payload: res.data
      })
    )
    .catch(err => {
      dispatch({
        type: CHECK_CONFIRM,
        payload: false
      })
    })
};

export const register = user => dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  console.log(user)
  axios
    .post('/api/auth/registration', user, config)
    .then(res =>
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data
      })
    )
    .catch(err => {
      dispatch(returnErrors(err.response.data, err.response.status, 'REGISTER_FAIL'));
      dispatch({
        type: REGISTER_FAIL
      })
    })
};

// Check token & load user
export const loadUser = () => (dispatch, getState) => {
  dispatch({ type: USER_LOADING });

  axios.get('/api/auth/user', tokenConfig(getState))
    .then(res => dispatch({
      type: USER_LOADED,
      payload: res.data
    }))
    .catch(err => {
      dispatch(returnErrors(err.response.data, err.response.status));
      dispatch({ type: AUTH_ERROR });
    });
};

export const tokenConfig = getState => {
  const token = getState().auth.token;

  const config = {
    headers: {
      "Content-type": "application/json"
    }
  };

  if(token) {
    config.headers['x-auth-token'] = token;
  }

  return config;
};