import axios from 'axios';
import { GET_ITEMS, GET_DONE_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING } from "./types";

export const getItems = userId => dispatch => {
  dispatch(setItemsLoading());
  axios
    .get(`api/task/actual/${userId}`)
    .then(res =>
      dispatch({
        type: GET_ITEMS,
        payload: res.data
      })
    )
};

export const getDoneItems = userId => dispatch => {
  dispatch(setItemsLoading());
  axios
    .get(`api/task/done/${userId}`)
    .then(res =>
      dispatch({
        type: GET_DONE_ITEMS,
        payload: res.data
      })
    )
};

export const deleteItem = id => dispatch  => {
  axios.delete(`api/task/${id}`).then(res =>
  dispatch({
    type: DELETE_ITEM,
    payload: id
  })
  )
};

export const addItem = item => dispatch => {
  axios
    .post('/api/task', item)
    .then(res =>
      dispatch({
        type: ADD_ITEM,
        payload: res.data
      })
    )
};

export const setItemsLoading = () => {
  return {
    type: ITEMS_LOADING
  }
};
