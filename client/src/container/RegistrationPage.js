import React, { Component } from 'react'
import RegistrationModal from "../components/Registration/RegistrationModal/RegistrationModal";
import UserList from "../components/Registration/UserList/UserList";

export default class Registration extends Component {
  render() {
    return(
      <div className="block">
        <h3>Регистрация</h3>
        <RegistrationModal/>
        <UserList />
      </div>
    );
  }
}
