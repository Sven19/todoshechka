import React, { Component } from 'react'
import TaskForm from "../components/Task/TaskForm/TaskForm";
import TaskList from "../components/Task/TaskList/TaskList";
import {Col, Row} from "antd";

export default class Task extends Component {
  render() {
    return(
      <Row>
        <Col span={16}>
          <div className="block">
            <TaskList />
          </div>
        </Col>

        <Col span={7} offset={1}>
          <div className="block">
            <TaskForm />
          </div>
        </Col>
      </Row>
    );
  }
}
