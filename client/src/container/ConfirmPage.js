import React, { Component } from 'react'
import Confirm from "../components/Auth/Confirm/Confirm";
import {Col} from "antd";

export default class Registration extends Component {
  render() {
    return(
      <Col span={10} offset={7}>
      <div className="block">
        <Confirm token={this.props.match.params.token} />
      </div>
      </Col>
    );
  }
}
