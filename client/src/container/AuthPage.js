import React, {Component, Fragment} from 'react'
import RegisterForm from "../components/Auth/RegisterForm/RegisterForm";
import LoginForm from "../components/Auth/LoginForm/LoginForm";
import styles from './AuthPage.module.css';
import { Tabs, Row, Col } from 'antd';
import connect from "react-redux/es/connect/connect";
import { clearErrors } from '../actions/errorActions';


const TabPane = Tabs.TabPane;



class AuthPage extends Component {

  callback = (key) => {
    //this.props.clearErrors();
  };

  render() {
    return(
      <Row>
        <Col span={14} >
          <h2 className={styles.slogan}>Тудушечка - отличный помощник в организации рабочего времени!</h2>
          <img src="../../image/logo.gif" style={{maxWidth: "100%"}}/>
        </Col>
        <Col span={8} offset={2}>
          <Tabs defaultActiveKey="1" onChange={this.callback} className="block">
            <TabPane tab="Вход" key="1"><div className={styles.formBody}><LoginForm /></div></TabPane>
            <TabPane tab="Регистрация" key="2"><div className={styles.formBody}><RegisterForm /></div></TabPane>
          </Tabs>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  error: state.error,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { clearErrors }
)(AuthPage);