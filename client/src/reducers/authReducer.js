import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  CHECK_CONFIRM
} from "../actions/types";

const initialState = {
  token: localStorage.getItem('token'),
  isAuthentificated: null,
  isLoading: false,
  isNewUser: false,
  isConfirm: null,
  user: null
};

export default function (state = initialState, action) {
  switch(action.type) {
    case CHECK_CONFIRM:
      return{
        ...state,
        isConfirm: action.payload
      };
    case USER_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case USER_LOADED:
      return{
        ...state,
        isAuthentificated: true,
        isLoading: false,
        user: action.payload
      };
    case LOGIN_SUCCESS:
      localStorage.setItem('token', action.payload.token);
      return{
        ...state,
        ...action.payload,
        isAuthentificated: true,
        isLoading: false,
      };
    case REGISTER_SUCCESS:
      return{
        ...state,
        isAuthentificated: false,
        isLoading: false,
        isNewUser: true,
      };
    case AUTH_ERROR:
    case LOGIN_FAIL:
    case REGISTER_FAIL:
    case LOGOUT_SUCCESS:
      localStorage.removeItem('token');
      return{
        ...state,
        token: null,
        user: null,
        isAuthentificated: false,
        isLoading: false
      };
    default:
      return state;
  }
}
